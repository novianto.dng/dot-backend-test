import express, { Application } from "express";
import Controller from "./utils/controller-interface";
import { AppDataSource } from "./utils/database";

class App {
  public express: Application;
  public port: number;

  constructor(controller: Controller[], port: number) {
    this.express = express();
    this.port = port;

    this.initializeMiddleware();
    this.intializeDatabase();
    this.initializeController(controller);
  }

  private initializeController(Controllers: Controller[]): void {
    Controllers.forEach((controller: Controller) => {
      this.express.use(controller.router);
    });
  }

  private initializeMiddleware(): void {
    this.express.use(express.json());
    // this.express.use(express.urlencoded({ extended: false }));
  }

  private intializeDatabase(): void {
    AppDataSource.initialize()
      .then(() => console.log("Connected to database"))
      .catch((err) => {
        console.log(err);
      });
  }

  public listen(): void {
    this.express.listen(this.port, () => {
      console.log(`App listen on port ${this.port}`);
    });
  }
}

export default App;
