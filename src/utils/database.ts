import { Post } from "../post/post-model";
import { DataSource } from "typeorm";
require("dotenv").config();

const AppDataSource = new DataSource({
  type: "postgres",
  host: process.env.DB_HOST || "localhost",
  port: 5432,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  entities: [Post],
  synchronize: true,
  logging: false,
});

export { AppDataSource };
