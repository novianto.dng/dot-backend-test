import { AppDataSource } from "../utils/database";
import { Post } from "./post-model";

class PostRepository {
  public async create(title: string, body: string, userId: number) {
    const post = new Post();

    post.body = body;
    post.title = title;
    post.user_id = userId;

    await AppDataSource.manager.save(post);

    return post;
  }
}

export default PostRepository;
