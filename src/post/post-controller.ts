import Controller from "../utils/controller-interface";
import { Router, Request, NextFunction, Response, response } from "express";
import { PostResponse, Post } from "./post-interface";
import axios from "axios";
import PostRepository from "./post-repository";

class PostController implements Controller {
  public path = "/posts";
  public router = Router();
  public repository = new PostRepository();

  constructor() {
    this.initializeRoute();
  }

  private initializeRoute(): void {
    this.router.get(this.path, this.index);
    this.router.get(`${this.path}/:id`, this.show);
    this.router.post(this.path, this.store);
    this.router.put(`${this.path}/:id`, this.update);
    this.router.patch(`${this.path}/:id`, this.patching);
    this.router.delete(`${this.path}/:id`, this.delete);
  }

  private async index(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Response | void> {
    try {
      axios
        .get("https://jsonplaceholder.typicode.com/posts")
        .then((posts: PostResponse) => res.status(200).json(posts.data));
    } catch (error) {
      console.log(error);
      res.send(error);
    }
  }

  private async show(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Response | void> {
    const postRipo = new PostRepository();

    try {
      const postid = req.params.id;
      let getPost: Post;
      axios
        .get(`https://jsonplaceholder.typicode.com/posts/${postid}`)
        .then(async (post: PostResponse) => {
          getPost = post.data;
          res.status(200).json(post.data);
          await postRipo.create(getPost.title, getPost.body, getPost.userId);
        });
    } catch (error) {
      console.log(error);
    }
  }

  private async store(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Response | void> {
    try {
      const { title, body, userId } = req.body;
      axios
        .post("https://jsonplaceholder.typicode.com/posts", {
          title,
          body,
          userId,
        })
        .then((post: PostResponse) => {
          res.status(201).json(post.data);
        });
    } catch (error) {
      console.log(error);
    }
  }

  private async update(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Response | void> {
    try {
      const { id, title, body, userId } = req.body;
      const postid = req.params.id;
      axios
        .put(`https://jsonplaceholder.typicode.com/posts/${postid}`, {
          title,
          body,
          userId,
          id,
        })
        .then((post: PostResponse) => {
          res.status(200).json(post.data);
        });
    } catch (error) {
      console.log(error);
    }
  }

  private async patching(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Response | void> {
    try {
      const { title } = req.body;
      const postid = req.params.id;
      axios
        .patch(`https://jsonplaceholder.typicode.com/posts/${postid}`, {
          title,
        })
        .then((post: PostResponse) => {
          res.status(200).json(post.data);
        });
    } catch (error) {
      console.log(error);
    }
  }

  private async delete(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Response | void> {
    try {
      const postid = req.params.id;
      axios
        .delete(`https://jsonplaceholder.typicode.com/posts/${postid}`)
        .then((post: PostResponse) => {
          res.status(200).json(post.data);
        });
    } catch (error) {
      console.log(error);
    }
  }
}

export default PostController;
