### Prerequesite

- Nodejs 16.13.1
- PostgresSQL 13.6

#### Runing Project

1. Go to project directory
2. Copy .example.env file and rename to .env and create your data base connection in there
3. Run <code>npm install</code> to install dependencies
4. Run <code> npm start </code> to run project
5. import postman collections on folder /doc
